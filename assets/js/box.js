function switchClassesBox() {
  const boxes = document.getElementsByClassName("box");

  for (i = 0; i < boxes.length; i++) {
    let box = boxes[i];
    if (box.classList.contains("bg-success")) {
      box.classList.remove("bg-success");
      box.classList.add("bg-danger");
    } else {
      box.classList.remove("bg-danger");
      box.classList.add("bg-success");
    }
  }

  // if (event.target.classList.contains("bg-success")) {
  //   event.target.classList.remove("bg-success");
  //   event.target.classList.add("bg-danger");
  // } else {
  //   event.target.classList.remove("bg-danger");
  //   event.target.classList.add("bg-success");
  // }
}
